package io.prods.engine.catalogue.test

import org.scalatest.{Matchers, WordSpec}

abstract class UnitSpec extends WordSpec with Matchers
