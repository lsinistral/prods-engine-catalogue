package io.prods.engine.catalogue.api

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.RouteResult
import akka.stream.ActorMaterializer
import akka.util.Timeout

object CatalogueServer extends SchemeRoutes {
  def apply(actorSystem: ActorSystem, host: String, port: Int) = {
    implicit val sys = actorSystem
    implicit val materializer = ActorMaterializer()
    implicit val ec = sys.dispatcher

    val bindingFuture = Http().bindAndHandle(route, host, port)
  }


}

