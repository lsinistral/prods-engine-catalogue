package io.prods.engine.catalogue.domain.entities.ProductScheme

import io.prods.engine.catalogue.domain.Types.MultiLang

/** Product scheme parameter **/
case class Parameter(fieldName: String, displayNames: MultiLang,
                     description: Option[MultiLang], weight: Double)


/** Product scheme **/
case class Scheme(schemeName: String,
                  parameters: List[Parameter],
                  deleted: Boolean = false)

object Scheme {

  def sample(givenName: String = "sample") = {
    val name = Parameter("name",
      Map("en" -> "Product Name",
        "ru" -> "Название Товара"),
      Some(Map("en" -> "One of the most common product parameters, which describes human readable name for the product")),
      0.0)

    val price = Parameter("price",
      Map("en" -> "Product Price",
        "ru" -> "Цена Товара"),
      Some(Map("en" -> "Amount of money payed for one item of the product")),
      0.0)

    Scheme(givenName, name :: price :: Nil)
  }
}

