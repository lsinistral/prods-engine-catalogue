package io.prods.engine.catalogue.api.marshaling

import io.prods.engine.catalogue.domain.entities.ProductScheme.{Parameter, Scheme}
import org.json4s.FieldSerializer._
import org.json4s.JsonAST.JField
import org.json4s.FieldSerializer

object SchemeMarshaling {
  val schemeSerializer = FieldSerializer[Scheme](
  ignore("deleted"), {
    case JField("scheme-name" | "sm", value) => JField("schemeName", value)
  }
  )
}

object SchemeParameterMarshaling {
  val schemeParameterSerializer = FieldSerializer[Parameter]()
}
trait SchemeMarshaling{
  import org.json4s.jackson
  import org.json4s.NoTypeHints
  import io.prods.engine.catalogue.api.marshaling
  import marshaling.SchemeMarshaling.schemeSerializer

  implicit val serialization = jackson.Serialization
  implicit val formats = jackson.Serialization.formats(NoTypeHints) + schemeSerializer + SchemeParameterMarshaling.schemeParameterSerializer
}