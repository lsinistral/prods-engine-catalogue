package io.prods.engine.catalogue.mongo.domain

import io.prods.engine.catalogue.domain.entities.ProductScheme.{Parameter, Scheme}
import io.prods.engine.catalogue.domain.Types.MultiLang
import reactivemongo.api.DB
import reactivemongo.bson._
import reactivemongo.extensions.dao.BsonDao
import reactivemongo.extensions.dao.Handlers._

import concurrent.ExecutionContext.Implicits.global
import scala.util.Success

object ProductSchemeHandlers {

  implicit object ParameterHandler extends BSONDocumentWriter[Parameter] with BSONDocumentReader[Parameter] {
    def write(parameter: Parameter): BSONDocument =
      BSONDocument(
        "fieldName" -> parameter.fieldName,
        "displayNames" -> parameter.displayNames,
        "description" -> parameter.description,
        "w" -> parameter.weight

      )

    def read(bson: BSONDocument): Parameter = {
      val description = bson.getAs[MultiLang]("description")

      val parameter = for {
        fieldName <- bson.getAsTry[String]("fieldName")
        displayNames <- bson.getAsTry[MultiLang]("displayNames")
        weight <- bson.getAsTry[Double]("w") orElse bson.getAsTry[Double]("weight") orElse Success(0.0)

      } yield Parameter(fieldName, displayNames, description, weight)
      parameter.get
    }
  }

  implicit object SchemeHandler extends BSONDocumentWriter[Scheme] with BSONDocumentReader[Scheme] {
    def write(scheme: Scheme): BSONDocument = BSONDocument(
      "_id" -> scheme.schemeName,
      "parameters" -> scheme.parameters,
      "deleted" -> scheme.deleted
    )

    def read(bson: BSONDocument): Scheme = {
      val scheme = for {
        schemeName <- bson.getAsTry[String]("_id")
        parameters <- bson.getAsTry[List[Parameter]]("parameters")
        deleted <- bson.getAsTry[Boolean]("deleted")  orElse Success(false)
      } yield Scheme(schemeName, parameters, deleted)
      scheme.get
    }

  }

}

trait MongoProductScheme {

  import ProductSchemeHandlers._

  val db: DB

  object SchemeDao extends BsonDao[Scheme, String](db, "catalogue.ProductSchemes")

}