package io.prods.engine.catalogue.domain

object Types {
  type MultiLang = Map[String, String]
  type ProductParameter = Map[String, Any]
}
