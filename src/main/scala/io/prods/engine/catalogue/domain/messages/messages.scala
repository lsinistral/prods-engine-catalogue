package io.prods.engine.catalogue.domain.messages

import io.prods.engine.catalogue.domain.entities.ProductScheme.Scheme

trait Createable[T]{
  case class create(item: T)
}
trait Showable[T]{
  case class show(item: T)
}
trait Updateable[U,T]{
  case class update(index: U, item: T)
}
trait Deletable[T]{
  case class delete(item: T)
}

trait BaseMessages[T] extends Createable[T] with Showable[T] with Updateable[T,T] with Deletable[T]



object ProductSchemeManagerMessages extends BaseMessages[Scheme]

object GenericMessages extends BaseMessages[Any]