package io.prods.engine.catalogue.test.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.{ContentTypes, HttpEntity}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import akka.stream.ActorMaterializer
import io.prods.engine.catalogue.api.SchemeRoutes
import io.prods.engine.catalogue.api.marshaling.SchemeMarshaling
import io.prods.engine.catalogue.domain.services.ProductSchemeService
import io.prods.engine.catalogue.mongo.MongoConnectionInstance
import io.prods.engine.catalogue.test.UnitSpec

class SchemeRoutesTest extends UnitSpec with ScalatestRouteTest with SchemeMarshaling {

  object schemeRoute extends SchemeRoutes

  implicit val sys = ActorSystem("testSystem")
  val schemeService = system.actorOf(ProductSchemeService.props(MongoConnectionInstance.db), "TestProductSchemeService")

  val route = schemeRoute.route
  "Scheme Route" should {
    "return a greeting to /test get request" in {
      Get("/test") ~> route ~> check {
        responseAs[String] should equal("hello")
      }
    }
    "create new scheme to valid /scheme post request, no scheme parameters" in {
      val ent = HttpEntity(ContentTypes.`application/json`,
        """{"scheme-name": "tests"}""")
      Post("/scheme", entity = ent) ~> route ~> check {
        responseAs[String] should equal("done")
      }
    }
    "create new scheme to valid /scheme post request, with scheme parameters" in {
      val ent = HttpEntity(ContentTypes.`application/json`,
        """{"scheme-name": "tests"}""")
      Post("/scheme", entity = ent) ~> route ~> check {
        responseAs[String] should equal("done")
      }
    }

  }
}
