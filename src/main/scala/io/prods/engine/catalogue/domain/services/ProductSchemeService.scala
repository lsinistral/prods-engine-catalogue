package io.prods.engine.catalogue.domain.services

import akka.actor.{Props, Actor, ActorLogging}
import io.prods.engine.catalogue.domain.entities.ProductScheme.{Parameter, Scheme}
import io.prods.engine.catalogue.mongo.domain.MongoProductScheme
import io.prods.engine.util.Settings
import reactivemongo.api.DB
import concurrent.ExecutionContext.Implicits.global
import akka.pattern.pipe

import scala.util.{Success, Failure}

class ProductSchemeService(passedDb: DB) extends Actor with ActorLogging with MongoProductScheme {

  import io.prods.engine.catalogue.domain.services.ProductSchemeService.Messages._

  val db = passedDb

  override def preStart() = {
    //    log.debug("starting product scheme service")
  }

  def receive = {
    case createSample(schemeName: String) =>
      //      log.debug("add sample Scheme given scheme name")
      val basicProductScheme = Scheme.sample()
      SchemeDao.insert(basicProductScheme)
    //      log.debug("inserted")
    case show(name: String) =>
      //      log.debug("get Scheme given name")
      val schm = SchemeDao.findById(name)
      schm pipeTo sender()

    case showAll() =>
      val schemes = SchemeDao.findAll()
      schemes pipeTo sender()

    case create(scheme: Scheme) =>
      //      log.debug("start inserting")
      val f = SchemeDao.insert(scheme)

    /*
          f.onComplete{
            case Failure(e) => log.debug(s"inserting failed: ${e.getMessage}")
            case Success(_) => log.debug("inserting done")
          }

    */


  }
}

object ProductSchemeService {
  def props(db: DB) = Props(new ProductSchemeService(db))

  trait Messages

  object Messages {

    case class create(item: Scheme) extends Messages

    case class createSample(schemeName: String) extends Messages

    case class show(schemeName: String) extends Messages

    case class showAll() extends Messages

  }

}