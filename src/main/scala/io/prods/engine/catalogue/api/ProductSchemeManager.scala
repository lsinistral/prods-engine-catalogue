package io.prods.engine.catalogue.api

import akka.actor._
import akka.http.scaladsl.marshalling.ToResponseMarshallable
import akka.http.scaladsl.model.headers
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.RequestContext
import de.heikoseeberger.akkahttpjson4s.Json4sSupport
import io.prods.engine.catalogue.api.DataCollector.{Cached, CacheCollected, Collected}
import io.prods.engine.catalogue.api.marshaling.SchemeMarshaling
import io.prods.engine.catalogue.domain.entities.ProductScheme.Scheme
import io.prods.engine.catalogue.domain.services.ProductSchemeService
import io.prods.engine.catalogue.mongo.{CacheMongoResults, MongoConnectionInstance}
import io.prods.engine.catalogue.api.marshaling.SchemeMarshaling
import io.prods.engine.catalogue.domain.services.ProductSchemeService.Messages


class ProductSchemeManager(ctx: RequestContext) extends Actor with ActorLogging with SchemeMarshaling with Json4sSupport {

  import ProductSchemeManager.Messages._

  override def preStart() = {
    //    log.debug("starting product scheme manager")
  }

  var receiver: ActorRef = context.parent

  def receive: Receive = {
    case Test =>
      sender() ! ctx.complete("hello")

    case createScheme(schemes: Seq[Scheme]) =>
      import Messages.create

      //      log.debug(self.toString())

      schemes.foreach(x => {
        val worker = getWorker
        worker ! create(x)
      })
      sender() ! ctx.complete("done")


    case showScheme(rawNames: String) =>
      import io.prods.engine.catalogue.domain.services.ProductSchemeService.Messages.{show, showAll}
      import ProductSchemeManager.cache
      receiver = sender()

      if (rawNames == "all") {
          val worker = getWorker
          worker ! showAll()
      }
      else {
        val names: Array[String] = rawNames.split(",")
        val collector = context.actorOf(DataCollector[Scheme, String](names.length))

        names.foreach { name =>
          if (cache.isCached(name)) {
            collector ! Some(cache.read(name))
          }
          else {
            val worker = getWorker
            collector !(name, worker)
            worker.tell(show(name), collector)
          }
        }
      }

    case Collected(x) =>
      complete(x)

    case x:List[Scheme] =>
      complete(x)

    case CacheCollected(map) =>
      import ProductSchemeManager.cache
      val mappedMap = map.asInstanceOf[Map[Either[Cached, String], Scheme]]
      complete(mappedMap.values.toList)
      mappedMap.foreach {
        case (key: Either[Cached, String], value: Scheme) =>
          key match {
            case Right(key) => {
              cache.add(key)(value)
            }
            case Left(x) =>
          }
      }
  }


  def getWorker = context.actorOf(ProductSchemeService.props(MongoConnectionInstance.db))

  def complete(comp: ToResponseMarshallable) = {
    receiver ! ctx.complete(comp)
    context.stop(self)
  }
}

object ProductSchemeManager {

  trait Messages

  object Messages {

    case object Test extends Messages

    case class createScheme(scheme: Seq[Scheme]) extends Messages

    case class showScheme(schemeName: String) extends Messages

  }

  val cache = CacheMongoResults[String, Scheme]

  def props(ctx: RequestContext) = Props(new ProductSchemeManager(ctx))

  def produceManager(unique: String, ctx: RequestContext)(implicit sys: ActorSystem): ActorRef =
    sys.actorOf(ProductSchemeManager.props(ctx), s"ProductSchemeManager-$unique")
}
