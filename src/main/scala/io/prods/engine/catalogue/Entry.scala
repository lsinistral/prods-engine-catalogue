package io.prods.engine.catalogue

import akka.actor.ActorSystem
import io.prods.engine.catalogue.api.CatalogueServer
import io.prods.engine.util.Settings


class Entry {
  val catalogueSystem = ActorSystem("prods-engine-catalogue")

  val catalogueServer = CatalogueServer(catalogueSystem,
    Settings.catalogueListenAddress,
    Settings.catalogueListenPort)


}