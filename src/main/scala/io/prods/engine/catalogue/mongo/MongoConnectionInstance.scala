package io.prods.engine.catalogue.mongo

import io.prods.engine.util.Settings

import scala.concurrent.ExecutionContext.Implicits.global

object MongoConnectionInstance {

  import reactivemongo.api.MongoDriver

  val driver = new MongoDriver
  val connection = driver.connection(List(Settings.mongoHost))
  val db = connection.db(Settings.mongoDatabase)

}
