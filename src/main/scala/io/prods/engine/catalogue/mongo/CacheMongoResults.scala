package io.prods.engine.catalogue.mongo

import akka.actor.ActorRef

import scala.collection.mutable.Map

class CacheMongoResults[K, V] {

  def isCached(key: K): Boolean = {cache.contains(key)}


  val cache: Map[K, V] = Map.empty


  def add(key: K)(value: V): Boolean = {
    if (!cache.contains(key)) {
      cache += (key -> value)
      true
    }
    else {
      false
    }
  }

  def get(key: K): Option[V] = {
    cache.get(key)
  }
  def read(key: K): V = {
    cache(key)
  }
}


object CacheMongoResults{
  def apply[K,V] = new CacheMongoResults[K,V]()
}



trait Collectable[K]{
  case class toCache[K](key:K, actor: ActorRef)
}