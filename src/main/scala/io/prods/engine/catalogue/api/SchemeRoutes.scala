package io.prods.engine.catalogue.api

import akka.actor.ActorSystem
import akka.http.scaladsl.model.headers.HttpOrigin
import akka.http.scaladsl.server.{RequestContext, RouteResult}
import akka.http.scaladsl.server.Route
import akka.util.Timeout
import de.heikoseeberger.akkahttpjson4s.Json4sSupport
import io.prods.engine.catalogue.api.ProductSchemeManager.Messages.{showScheme, createScheme, Test}
import io.prods.engine.catalogue.api.ProductSchemeManager.Messages
import io.prods.engine.catalogue.api.marshaling.SchemeMarshaling
import io.prods.engine.catalogue.domain.entities.ProductScheme.Scheme
import scala.concurrent.{Future, ExecutionContext}
import akka.http.scaladsl.server.Directives._
import akka.pattern.ask
import akka.stream.Materializer

trait SchemeRoutes extends SchemeMarshaling with Json4sSupport {

  import scala.concurrent.duration.SECONDS
  import akka.http.scaladsl.model.headers

  implicit val timeout = Timeout(300, SECONDS)


  def route(implicit ec: ExecutionContext, mat: Materializer, sys: ActorSystem) = {
    respondWithHeaders(
      headers.`Access-Control-Allow-Origin`(HttpOrigin("http://localhost:4200")),
      headers.`Access-Control-Allow-Headers`("Content-Type")
    ) {
      path("test") {
        sendToManager(Test)
      } ~
        pathPrefix("schemes") {
          pathEnd {
            post {
              entity(as[Scheme]) { scm =>
                sendToManager(createScheme(List(scm)))
              } ~
                entity(as[List[Scheme]]) { scm =>
                  sendToManager(createScheme(scm))
                }
            }
          } ~ // TODO: tests for Get Requests
            get {
              pathPrefix(Segment) { path =>
                sendToManager(showScheme(path))
              } ~
                (pathSingleSlash | pathEnd) {
                  sendToManager(showScheme("all"))
                }
            }
        }~
      options{
        complete("")
      }
    }
  }

  def sendToManager(action: Messages)(implicit ec: ExecutionContext, sys: ActorSystem): Route = (ctx: RequestContext) => {
    import io.prods.engine.catalogue.api.ProductSchemeManager.produceManager

    val manager = produceManager(ctx.hashCode().toString, ctx)
    val answer = manager ? action
    answer.mapTo[Future[RouteResult]].flatMap(identity)
  }
}



