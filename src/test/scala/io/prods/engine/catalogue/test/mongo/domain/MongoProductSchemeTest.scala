package io.prods.engine.catalogue.test.mongo.domain

import io.prods.engine.catalogue.domain.entities.ProductScheme.{Scheme, Parameter}
import io.prods.engine.catalogue.mongo.domain.ProductSchemeHandlers
import io.prods.engine.catalogue.test.UnitSpec
import reactivemongo.bson.{BSONObjectID, BSON, BSONDocument, BSONArray}
import reactivemongo.extensions.dao.Handlers._

class MongoProductSchemeTest extends UnitSpec {

  object ParameterFixtures {
    val bsonDoubleWParameter = BSONDocument(
      "fieldName" -> "name",
      "displayNames" -> Map("en" -> "Product Name", "ru" -> "Название товара"),
      "description" -> Map("en" -> "Human readable name for the product"),
      "w" -> 0.0 // or BSONDouble(0)
    )

    val bsonDoubleWeightParameter = BSONDocument(
      "fieldName" -> "name",
      "displayNames" -> Map("en" -> "Product Name", "ru" -> "Название товара"),
      "description" -> Map("en" -> "Human readable name for the product"),
      "weight" -> 0.0
    )

    /* price parameter
    val bsonDoubleWeightParameter  = BSONDocument(
      "fieldName" -> "price",
      "displayNames" -> Map("en" -> "Product Price", "ru" -> "Цена Товара"),
      "description" -> Map("en" -> "Amount of money payed for one item of the product"),
      "weight" -> 0.0
    )*/

    val validModelParameter = Parameter("name",
      Map("en" -> "Product Name", "ru" -> "Название товара"),
      Some(Map("en" -> "Human readable name for the product")), 0)

    /* price parameter
    val modelParameterPrice = Parameter("price",
      Map("en" -> "Product Price", "ru" -> "Цена Товара"),
      Map("en" -> "Amount of money payed for one item of the product"), 0)*/

  }

  "ParameterHandler" should {

    "convert valid Parameter to valid BSON" in {
      import ParameterFixtures._
      val doc: BSONDocument = BSON.writeDocument(validModelParameter)(ProductSchemeHandlers.ParameterHandler)
      doc shouldBe bsonDoubleWParameter
    }

    "convert valid BSON to valid Parameter" in {
      import ParameterFixtures._
      val param: Parameter = BSON.readDocument[Parameter](bsonDoubleWParameter)(ProductSchemeHandlers.ParameterHandler)
      param shouldBe validModelParameter
    }

    "convert BSON with no Weight to valid Parameter with Zero Weight" in {
      import ParameterFixtures._
      val bsonNoWeightParameter = BSONDocument(
        "fieldName" -> "name",
        "displayNames" -> Map("en" -> "Product Name", "ru" -> "Название товара"),
        "description" -> Map("en" -> "Human readable name for the product")
      )

      val param = BSON.readDocument[Parameter](bsonNoWeightParameter)(ProductSchemeHandlers.ParameterHandler)
      param shouldBe validModelParameter
    }

    "convert BSON with no Description to valid Parameter" in {
      val bsonNoDescriptionParameter = BSONDocument(
        "fieldName" -> "name",
        "displayNames" -> Map("en" -> "Product Name", "ru" -> "Название товара"),
        "w" -> 0.0 // or BSONDouble(0)
      )

      val validModelNoDescriptionParameter = Parameter("name",
        Map("en" -> "Product Name", "ru" -> "Название товара"),
        None, 0)

      val param = BSON.readDocument[Parameter](bsonNoDescriptionParameter)(ProductSchemeHandlers.ParameterHandler)
      param shouldBe validModelNoDescriptionParameter
    }

    "throw DocumentKeyNotFound when invalid BSON with no Name" in {
      import reactivemongo.bson.exceptions.DocumentKeyNotFound

      val bsonInvalidNoNameParameter = BSONDocument(
        "displayNames" -> Map("en" -> "Product Name", "ru" -> "Название товара"),
        "description" -> Map("en" -> "Human readable name for the product"),
        "w" -> 0.0 // or BSONDouble(0)
      )

      an[DocumentKeyNotFound] should be thrownBy BSON.readDocument[Parameter](bsonInvalidNoNameParameter)(ProductSchemeHandlers.ParameterHandler)

    }
  }

  object SchemeFixtures {
    val bsonScheme = BSONDocument(
      "_id" -> "sample",
      "parameters" -> BSONArray(
        ParameterFixtures.bsonDoubleWParameter,
        ParameterFixtures.bsonDoubleWParameter
      ),
      "deleted" -> true
    )
    val modelScheme = Scheme("sample", ParameterFixtures.validModelParameter :: ParameterFixtures.validModelParameter :: Nil, deleted = true)
  }

  "SchemeHandler" should {

    "convert valid Scheme to valid BSON" in {
      import SchemeFixtures._
      val doc = BSON.writeDocument(modelScheme)(ProductSchemeHandlers.SchemeHandler)


      doc shouldBe bsonScheme

    }

    "convert valid BSON to valid Scheme" in {
      import SchemeFixtures._
      val scheme = BSON.readDocument[Scheme](bsonScheme)(ProductSchemeHandlers.SchemeHandler)
      scheme shouldBe modelScheme
    }

    "convert BSON with no Deleted flag to valid Scheme with Deleted flag set to False" in {
      val bsonScheme = BSONDocument(
        "_id" -> "sample",
        "parameters" -> BSONArray(
          ParameterFixtures.bsonDoubleWParameter,
          ParameterFixtures.bsonDoubleWParameter
        )
      )
      val modelScheme = Scheme("sample", ParameterFixtures.validModelParameter :: ParameterFixtures.validModelParameter :: Nil, deleted = false)

      val scheme = BSON.readDocument[Scheme](bsonScheme)(ProductSchemeHandlers.SchemeHandler)
      scheme shouldBe modelScheme
    }

    "throw DocumentKeyNotFound when invalid BSON with no Name" in {
      import reactivemongo.bson.exceptions.DocumentKeyNotFound
      val noNameBsonScheme = BSONDocument(
        "parameters" -> BSONArray(
          ParameterFixtures.bsonDoubleWParameter,
          ParameterFixtures.bsonDoubleWParameter
        ),
        "deleted" -> true

      )
      an[DocumentKeyNotFound] should be thrownBy BSON.readDocument[Scheme](noNameBsonScheme)(ProductSchemeHandlers.SchemeHandler)
    }

    "throw DocumentKeyNotFound when invalid BSON with no Parameters" in {
      import reactivemongo.bson.exceptions.DocumentKeyNotFound
      val noNameBsonScheme = BSONDocument(
        "_id" -> "Sample",
        "deleted" -> true

      )
      an[DocumentKeyNotFound] should be thrownBy BSON.readDocument[Scheme](noNameBsonScheme)(ProductSchemeHandlers.SchemeHandler)
    }
  }
}
