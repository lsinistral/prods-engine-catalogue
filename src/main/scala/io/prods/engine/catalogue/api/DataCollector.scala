package io.prods.engine.catalogue.api

import akka.actor._
import io.prods.engine.catalogue.api.DataCollector.{CacheCollected, Collected}
import scala.collection.mutable
import scala.reflect.ClassTag

abstract class DataCollector[V: ClassTag] extends Actor {
  val itemsToCollect: Int
  val seq = mutable.MutableList.empty[V]
  var itemsCollected = 0

  def receive = {
    case Some(value: V) =>
      success(value, sender())
    case None =>
      fail(sender())
  }

  def fail(sender: ActorRef): Unit = {
    itemsCollected += 1
    sender ! PoisonPill
    flush()
  }

  def success(value: V, sender: ActorRef): Unit

  def flush() = {
    if (itemsCollected == itemsToCollect)
      context.parent ! Collected[V](seq.toList)
  }
}

class SimpleDataCollector[V: ClassTag](override val itemsToCollect: Int) extends DataCollector[V] {
  def success(value: V, sender: ActorRef): Unit = {
    seq += value
    itemsCollected += 1
    sender ! PoisonPill
    flush()
  }
}

class CachedDataCollector[V: ClassTag, K: ClassTag](override val itemsToCollect: Int) extends DataCollector[V] {
  import DataCollector.Cached

  type CachedKey = Either[Cached, K]
  var responsesToCache = Map.empty[ActorRef, K]
  val cachedSeq = mutable.Map.empty[CachedKey, V]

  override def receive = super.receive.orElse {
    case (key: K, actor: ActorRef) =>
      responsesToCache = responsesToCache + (actor -> key)
  }

  def success(value: V, sender: ActorRef): Unit = {
    val item: (CachedKey, V) = if (responsesToCache.contains(sender)) {
      sender ! PoisonPill
      Right(responsesToCache(sender)) -> value
    }
    else {
      Left(Cached(value.hashCode())) -> value
    }
    cachedSeq += item
    itemsCollected += 1
    flush()
  }

  override def flush() = {
    if (itemsCollected == itemsToCollect) {
      context.parent ! CacheCollected[CachedKey, V](cachedSeq.toMap)
    }
  }
}


object DataCollector {

  case class Cached(id: Int)

  case class Collected[V](items: List[V])

  case class CacheCollected[K, V](items: Map[K, V])

  def apply[V: ClassTag](itemsCount: Int) = Props(new SimpleDataCollector[V](itemsCount))

  def apply[V: ClassTag, K: ClassTag](itemsCount: Int) = Props(new CachedDataCollector[V, K](itemsCount))

}
